'use strict';

const ItemEditDefinition = {
    bindings: {
        items: '='
    },
    templateUrl: '/js/components/item/item-edit.html',
    transclude: true,
    controller: ItemEditController
};

/** @ngInject */
function ItemEditController($scope, $rootScope, $routeParams, $location, dataFactory) {
    var vm = this;

    vm.items = dataFactory.getItemsAll();
    vm.item = {};
    vm.saveItem = saveItem;

    if ($routeParams.id !== undefined) {
        vm.item = dataFactory.getItem($routeParams.id);

        if (undefined === vm.item || null === vm.item) {
            console.log('item with this id not found');
            //$location.path('/photos/photo-add').replace();
        }
    }

    function saveItem(item) {
        dataFactory.save(item);
        $location.path('/photos').replace();
    };
};

angular
    .module('PhotoGalleryApp')
    .component('itemEdit', ItemEditDefinition);