'use strict';

const ItemAddDefinition = {
    bindings: {
        item: '='
    },
    templateUrl: '/js/components/item/item-add.html',
    transclude: true,
    controller: ItemAddController
};

/** @ngInject */
function ItemAddController($scope, $rootScope, $routeParams, $location, dataFactory) {
    var vm = this;

    vm.item = {};
    vm.generateId = generateId;
    vm.saveItem = saveItem;


    function generateId() {
        return '_' +  Math.random().toString(36).substr(2, 9);
    }

    function saveItem() {
        vm.item.id = generateId();
        dataFactory.addItem(vm.item);
        $location.path('/photos').replace();
    };
};

angular
    .module('PhotoGalleryApp')
    .component('itemAdd', ItemAddDefinition);