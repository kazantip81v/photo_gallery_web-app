//(function() {
	'use strict';

	angular
		.module('PhotoGalleryApp')
		.factory('dataFactory', dataFactory);

	dataFactory.$inject = ['$http', '$rootScope'];

	function dataFactory($http, $rootScope) {
		var items = [];
		var service = {
			getItemsAll: getItemsAll,
			getItem: getItem,
			addItem: addItem,
			updateItem: updateItem,
			deleteItem: deleteItem,
			save: save
		};

		getItems();

		function getItems() {
			$http({method: 'GET', url: 'https://jsonplaceholder.typicode.com/photos'})
				.then(
					function(response) {
						items = response.data;
						$rootScope.$broadcast('items:updated');
						console.log('Yes - successful receipt of the database');
					},
					function(error) {
						console.log(error, "Sorry - database load failed");
					}
				);
		}

		function getItemsAll() {
			return items;
		};

		function getItem(id) {
			var item = null;

			angular.forEach(items, function(value) {
				if (parseInt(value.id) === parseInt(id)) {
					item = value;
					return false;
				}
			});

			return item;
		};

		function addItem(item) {
			$http({method: 'POST', url: 'https://jsonplaceholder.typicode.com/photos', data: item})
				.then(
					function(response) {
						items.push(response.data);
						$rootScope.$broadcast('item:added', response.data);
						console.log('added - ' + JSON.stringify(items[items.length - 1]) );
					},
					function(error) {
						$rootScope.$broadcast('item:error', error.data);
					}
				);
		};

		function updateItem(item) {
			$http({method: 'PUT', url: 'https://jsonplaceholder.typicode.com/photos/' + item.id, data: item})
				.then(
					function(response) {
						$rootScope.$broadcast('item:updated', response.data);
						console.log('update - ' + JSON.stringify(item));
					},
					function(error) {
						$rootScope.$broadcast('item:error', error.data);
					}
				);
		};

		function deleteItem(item) {
			$http({method: 'DELETE', url: 'https://jsonplaceholder.typicode.com/photos/' + item.id})
				.then(
					function(response) {
						angular.forEach(items, function(value, i) {
							if (parseInt(value.id) === parseInt(item.id)) {
								console.log('delete - ' + JSON.stringify( items[i]) );
								items.splice(i, 1);

								return false;
							}
						});
						$rootScope.$broadcast('item:deleted', response.data);
					},
					function(error) {
						$rootScope.$broadcast('item:error', error.data);
					}
				);
		};

		function save(item) {
			if (undefined !== item.id && parseInt(item.id) > 0) {
				service.updateItem(item);
			}
			else {
				service.addItem(item);
			}
		};

		return service;
	};

//})();