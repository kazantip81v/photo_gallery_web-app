//(function() {

	'use strict';

	angular
		.module('PhotoGalleryApp')
		.controller('MyAppController', MyAppController);

	MyAppController.$inject = ['$scope', '$rootScope', '$location', 'dataFactory'];

	function MyAppController($scope, $rootScope, $location, dataFactory) {
		var main = this;

		main.items = dataFactory.getItemsAll();

		main.isActiveNav = isActiveNav;
		main.locationPath = '';
		main.photosInFolder = false;
		main.folderPhotoLimit = 0;

		function isActiveNav(viewLocation) {
			(viewLocation === $location.path()) ? main.locationPath = $location.path() : '';

			return viewLocation === $location.path();
		};

		$rootScope.$on('folders:create', function(event, data) {
			main.fotosInFolder = data[0];
			main.folderPhotoLimit = data[1];
		});

		$rootScope.$on('item:deleted', function() {
			main.items = dataFactory.getItemsAll();
		});
	}

//})();
