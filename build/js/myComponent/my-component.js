'use strict';

const myComponentDefinition = {
    bindings: {
        myInfo: '<'
    },
    templateUrl: '/js/myComponent/my-component.html',
    transclude: true,
    controller: MyController
};

/** @ngInject */
function MyController($http) {
    var vm = this;

    $http
        .get('/js/myComponent/myInfo.json')
        .then(function (response) {
            vm.myInfo = response.data;
        });
};

angular
    .module('PhotoGalleryApp')
    .component('myComponent', myComponentDefinition);

