'use strict';

const FoldersDefinition = {
    bindings: {
        folder: '='
    },
    templateUrl: 'js/components/folder/folders.html',
    transclude: true,
    controller: FoldersController,
};

/** @ngInject */
function FoldersController($scope, $rootScope, dataFactory) {
    var vm = this;

    vm.items = dataFactory.getItemsAll();

    vm.folderLimit = 5;
    vm.getFolders = getFolders;
    vm,getFoldersCollection = getFoldersCollection;
    vm.foldersCollection = getFoldersCollection(vm.items);

    function getFolders(arr) {
        let used = {};

        return arr.reduce(function(folders, obj) {
            !(obj.albumId in used) && folders.push(obj.albumId);
            used[obj.albumId] = 0;

            return folders;
        }, []);
    };


    function getFoldersCollection(items) {
        let foldersCollection = [];
        let folders = getFolders(items);

        folders.forEach(function(item, i, arr) {
            let foto = [];

            items.forEach(function(obj) {
                arr[i] === obj.albumId ? foto.push(obj) : 0;
            });
            foldersCollection.push(foto);
        });

        return foldersCollection;
    };

    $rootScope.$emit('folders:create', [vm.foldersCollection, vm.folderLimit]);
};

angular
    .module('PhotoGalleryApp')
    .component('folders', FoldersDefinition);