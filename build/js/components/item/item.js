'use strict';

const PhotoItemDefinition = {
    bindings: {
        folder: '=',
        item: '='
    },
    require: {
        parent: '^folders'
    },
    templateUrl: '/js/components/item/item.html',
    transclude: true,
    controller: PhotoItemController
};

/** @ngInject */
function PhotoItemController($scope, $rootScope, $location, dataFactory) {
    var vm = this;

    vm.items = dataFactory.getItemsAll();

    vm.itemLimit = 8;
    vm.deleteItem = deleteItem;

    function deleteItem(item) {
        dataFactory.deleteItem(item);
    }
};

angular
    .module('PhotoGalleryApp')
    .component('photoItem', PhotoItemDefinition);