;(function(){
	'use strict';

	$(document).ready(function(){

		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
			$('body').addClass('ios');
		} else{
			$('body').addClass('web');
		};

		$('body').removeClass('loaded');

	});

	/* viewport width */
	function viewport() {
		var e = window;
		var	a = 'inner';

		if ( !('innerWidth' in window) ) {
			a = 'client';
			e = document.documentElement || document.body;
		}

		return {
			width : e[ a+'Width' ],
			height : e[ a+'Height' ]
		};
	};
	/* viewport width */

	var handler = function() {
		var viewport_wid = viewport().width;
		var viewport_height = viewport().height;
	};

	$(window).bind('load', handler);
	$(window).bind('resize', handler);

})();