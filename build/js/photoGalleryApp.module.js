(function() {
	'use strict';

	angular
		.module('PhotoGalleryApp', ['ngRoute'])
		.config(config);

	config.$inject = ['$routeProvider', '$locationProvider'];

	function config($routeProvider, $locationProvider) {
		$locationProvider.hashPrefix('');
		/*$locationProvider.html5Mode(true);*/

		$routeProvider.
			when('/welcome', {
				templateUrl: 'template/welcome.html',
				controller: MyAppController
			}).
			when('/photos', {
				templateUrl: 'template/photosTemplate.html'
				//controller: MyAppController
			}).
			when('/videos', {
				templateUrl: 'template/videosTemplate.html'
				//controller: MyAppController
			}).
			when('/projects', {
				templateUrl: 'template/projectsTemplate.html'
				//controller: MyAppController
			}).
			when('/photos/photo-edit/:id', {
				templateUrl: 'template/itemEditTemplate.html'
				//controller: MyAppController
			}).
			when('/photos/photo-add', {
				templateUrl: 'template/itemAddTemplate.html'
				//controller: MyAppController
			}).
			otherwise({
				redirectTo: '/welcome'
			});
	}

})();