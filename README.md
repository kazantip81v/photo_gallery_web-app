#JavaScript Angular.js developer - test
It's test task for the position of the JavaScript developer (Angular)

Photo Gellary Web-App

##Quick start

* Install dev-dependencies `npm i`
* Install dependencies `bower i`
* Launch `gulp` to run watchers, server, compilers and build

##Directory Layout

	interact                    # Project root
	├── /build/                 # Minified files for production 50/50 ))
	├── /src/                   # Source files for developing
	├── bower.json              # List of 3rd party libraries and utilities
	├── package.json            # Dependencies for node.js
	├── gulpfile.js             # gulp.js config
	├── LICENSE                 # License file
	├── README.md               # File you read
